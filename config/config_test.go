package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v2"
)

const queuebeat = `
queuebeat:
  smartthings:
    type: sqs
    region: us-east-1
    queue_url: https://sqs.us-east-1.aws.com/test
    wait_timeout_sec: 20
    visibility_timeout_sec: 30
    aws_access_key: key
    aws_secret_key: secret
    fields:
      env: production
    filters:
      json:
        fields:
          - Message
      grok:
        field: Message.Description
        pattern: '%{TYPE} %{DESC}'
    tags:
      - ec2

  logging:
    type: Kafka
    topic: logs
`

func TestUnmarshal(t *testing.T) {
	c := Config{}
        err := yaml.Unmarshal([]byte(queuebeat), &c)
	if err != nil {
		t.Fatal(err)
	}

	if assert.Len(t, c.Queuebeat.SQS, 1) {
		sqs := c.Queuebeat.SQS[0]
		assert.Equal(t, "smartthings", sqs.Name)
		assert.Equal(t, "sqs", sqs.Type)
		assert.Equal(t, "us-east-1", sqs.Region)
		assert.Equal(t, "https://sqs.us-east-1.aws.com/test", sqs.QueueURL)
		assert.Equal(t, int64(20), sqs.WaitTimeoutSec)
		assert.Equal(t, int64(30), sqs.VisibilityTimeoutSec)
		assert.Contains(t, sqs.Tags, "ec2")
		assert.Len(t, sqs.Fields, 1)
		assert.Len(t, sqs.Filters, 2)
	}

	if assert.Len(t, c.Queuebeat.Kafka, 1) {
		kafka := c.Queuebeat.Kafka[0]
		assert.Equal(t, "logging", kafka.Name)
		assert.Equal(t, "Kafka", kafka.Type)
		assert.Equal(t, "logs", kafka.Topic)
	}

	t.Logf("Config %+v\n", c)
}
