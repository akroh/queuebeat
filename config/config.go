// Config is put into a different package to prevent cyclic imports in case
// it is needed in several locations

package config

import (
	"fmt"
	"strings"

	"github.com/mitchellh/mapstructure"
)

const (
	DefaultRegion = "us-east-1"
	DefaultVisibilityTimeoutSec int64 = 30
	DefaultWaitTimeoutSec int64 = 20
)

type Validator interface {
	Validate() bool
}

type Config struct {
	Queuebeat Queuebeat
}

type Queuebeat struct {
	SQS   []SQS
	Kafka []Kafka
}

func (qb *Queuebeat) UnmarshalYAML(unmarshall func(interface{}) error) error {
	var value map[string]interface{}
	if err := unmarshall(&value); err != nil {
		return err
	}

	for name, ms := range value {
		var qt queueType
		err := mapstructure.Decode(ms, &qt)
		if err != nil {
			return err
		}

		switch strings.ToLower(qt.Type) {
		default:
			return fmt.Errorf("unknown queue type '%s'", qt.Type)
		case "sqs":
			var sqs SQS
			sqs.Region = DefaultRegion
			sqs.VisibilityTimeoutSec = DefaultVisibilityTimeoutSec
			sqs.WaitTimeoutSec = DefaultWaitTimeoutSec
			err := Decode(ms, &sqs)
			if err != nil {
				return err
			}
			sqs.Name = name
			qb.SQS = append(qb.SQS, sqs)
		case "kafka":
			var kafka Kafka
			err := Decode(ms, &kafka)
			if err != nil {
				return err
			}
			kafka.Name = name
			qb.Kafka = append(qb.Kafka, kafka)
		}
	}

	return nil
}

func Decode(in interface{}, out interface{}) error {
	config := &mapstructure.DecoderConfig{
		ErrorUnused: true,
		Result:      out,
	}
	dec, err := mapstructure.NewDecoder(config)
	if err != nil {
		return err
	}

	err = dec.Decode(in)
	if err != nil {
		return err
	}

	return nil
}

type Tags []string

type Fields map[string]interface{}

type Filters map[string]interface{}

type queueType struct {
	Type string `mapstructure:"type"`
}

type Queue struct {
	Name    string
	Type    string
	Fields  Fields  `mapstructure:"fields"`
	Filters Filters `mapstructure:"filters"`
	Tags    Tags    `mapstructure:"tags"`
}

type SQS struct {
	Queue                `mapstructure:",squash"`
	Region               string `mapstructure:"region"`
	QueueURL             string `mapstructure:"queue_url"`
	VisibilityTimeoutSec int64  `mapstructure:"visibility_timeout_sec"`
	WaitTimeoutSec       int64  `mapstructure:"wait_timeout_sec"`
	AWSAccessKey         string `mapstructure:"aws_access_key"`
	AWSSecretKey         string `mapstructure:"aws_secret_key"`
}

type Kafka struct {
	Queue `mapstructure:",squash"`
	Topic string
}
