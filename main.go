package main

import (
	"os"

	"github.com/elastic/beats/libbeat/beat"

	"github.com/andrewkroh/queuebeat/beater"
)

func main() {
	err := beat.Run("queuebeat", "", beater.New())
	if err != nil {
		os.Exit(1)
	}
}
