# Queuebeat

Welcome to Queuebeat.

Ensure that this folder is at the following location:
`${GOPATH}/github.com/andrewkroh`

## Getting Started with Queuebeat

### Init Project
To get running with Queuebeat, run the following commands:

```
glide update --no-recursive
make update
```


To push Queuebeat in the git repository, run the following commands:

```
git init
git add .
git commit
git remote set-url origin https://github.com/andrewkroh/queuebeat
git push origin master
```

For further development, check out the [beat developer guide](https://www.elastic.co/guide/en/beats/libbeat/current/new-beat.html).

### Build

To build the binary for Queuebeat run the command below. This will generate a binary
in the same directory with the name queuebeat.

```
make
```


### Run

To run Queuebeat with debugging output enabled, run:

```
./queuebeat -c queuebeat.yml -e -d "*"
```


### Test

To test Queuebeat, run the following commands:

```
make testsuite
```

alternatively:
```
make unit-tests
make system-tests
make integration-tests
make coverage-report
```

The test coverage is reported in the folder `./build/coverage/`


### Update

Each beat has a template for the mapping in elasticsearch and a documentation for the fields
which is automatically generated based on `etc/fields.yml`.
To generate etc/queuebeat.template.json and etc/queuebeat.asciidoc

```
make update
```


### Cleanup

To clean  Queuebeat source code, run the following commands:

```
make fmt
make simplify
```

To clean up the build directory and generated artifacts, run:

```
make clean
```


### Clone

To clone Queuebeat from the git repository, run the following commands:

```
mkdir -p ${GOPATH}/github.com/andrewkroh
cd ${GOPATH}/github.com/andrewkroh
git clone https://github.com/andrewkroh/queuebeat
```


For further development, check out the [beat developer guide](https://www.elastic.co/guide/en/beats/libbeat/current/new-beat.html).
