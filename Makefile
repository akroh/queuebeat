BEATNAME=queuebeat
BEAT_DIR=github.com/andrewkroh
SYSTEM_TESTS=false
TEST_ENVIRONMENT=false
ES_BEATS=./vendor/github.com/elastic/beats
GOPACKAGES=$(shell glide novendor)
PREFIX?=.

# Path to the libbeat Makefile
-include $(ES_BEATS)/libbeat/scripts/Makefile

.PHONY: init
init:
	glide update  --no-recursive
	make update
	git init
	git add .

.PHONY: install-cfg
install-cfg:
	mkdir -p $(PREFIX)
	cp etc/queuebeat.template.json     $(PREFIX)/queuebeat.template.json
	cp etc/queuebeat.yml               $(PREFIX)/queuebeat.yml
	cp etc/queuebeat.yml               $(PREFIX)/queuebeat-linux.yml
	cp etc/queuebeat.yml               $(PREFIX)/queuebeat-binary.yml
	cp etc/queuebeat.yml               $(PREFIX)/queuebeat-darwin.yml
	cp etc/queuebeat.yml               $(PREFIX)/queuebeat-win.yml

.PHONY: update-deps
update-deps:
	glide update  --no-recursive
