package filter

import (
	"github.com/elastic/beats/libbeat/common"
	"github.com/elastic/beats/libbeat/logp"
	"github.com/gemsi/grok"
)

type GrokFilter struct {
	baseFilter `mapstructure:",squash"`
	Field      string `mapstructure:"field"`
	Pattern    string `mapstructure:"pattern"`
}

func (f GrokFilter) Filter(e common.MapStr) common.MapStr {
	logp.Debug("grok", "grok'ing field=%d, pattern=%s, data=%+v", e)

	v, found := e[f.Field]
	if !found {
		return e
	}

	text, ok := v.(string)
	if !ok {
		return e
	}

	g := grok.New()
	values, err := g.ParseTyped(f.Pattern, text)
	if err != nil {
		return e
	}

	for k, v := range values {
		e[k] = v
	}

	return e
}
