package filter

import (
	"encoding/json"
	"strings"

	"github.com/elastic/beats/libbeat/common"
)

type JsonFilter struct {
	baseFilter      `mapstructure:",squash"`
	Fields []string `mapstructure:"fields"`
}

func (f JsonFilter) Filter(e common.MapStr) common.MapStr {
	for _, field := range f.Fields {
		rawValue, ok := e[field]
		if !ok {
			continue
		}

		str, ok := rawValue.(string)
		if !ok {
			continue
		}

		structuredData := make(map[string]interface{})
		err := unmarshal([]byte(str), structuredData)
		if err != nil {
			continue
		}

		e[field] = structuredData
	}
	return e
}

func unmarshal(b []byte, m map[string]interface{}) error {
	err := json.Unmarshal(b, &m)
	if err == nil {
		err = jsonStringsToObject(m)
	}
	return err
}

func jsonStringsToObject(m map[string]interface{}) error {
	for k, v := range m {
		switch v.(type) {
		case string:
			valueStr := v.(string)
			if strings.HasPrefix(valueStr, "{") && strings.HasSuffix(valueStr, "}") {
				valueMap := make(map[string]interface{})
				err := unmarshal([]byte(valueStr), valueMap)
				if err == nil {
					m[k] = valueMap
				}
			}
		case map[string]interface{}:
			valueMap := v.(map[string]interface{})
			jsonStringsToObject(valueMap)
		}
	}
	return nil
}
