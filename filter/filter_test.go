package filter

import (
	"testing"
	"github.com/stretchr/testify/assert"
	"encoding/json"
)

func TestEmptyFilter(t *testing.T) {
	chain, err := NewFilterChain(nil)

	if assert.NoError(t, err) {
		assert.NotNil(t, chain)
	}
}

func TestJSONFilter(t *testing.T) {
	ms := map[string]interface{}{
		"json": map[string]interface{}{
			"fields": []string{"message"},
		},
	}
	chain, err := NewFilterChain(ms)

	if assert.NoError(t, err) {
		assert.Len(t, chain.filters, 1)
	}

	mapStr:= map[string]interface{}{
		"message": `{"data": "this is json"}`,
	}

	js := chain.Filter(mapStr)

	message, ok := js["message"]
	assert.True(t, ok)

	messageMapStr, ok := message.(map[string]interface{})
	assert.True(t, ok)

	data, ok := messageMapStr["data"]
	assert.True(t, ok)

	assert.Equal(t, "this is json", data)
	t.Logf("JSON %+v\n", js)
}

func TestGrokFilter(t *testing.T) {
	ms := map[string]interface{}{
		"grok": map[string]interface{}{
			"field": "message",
			"pattern": "%{COMMONAPACHELOG}",
		},
	}
	chain, err := NewFilterChain(ms)

	if assert.NoError(t, err) {
		assert.Len(t, chain.filters, 1)
	}

	mapStr:= map[string]interface{}{
		"message": `127.0.0.1 - - [23/Apr/2014:22:58:32 +0200] "GET /index.php HTTP/1.1" 404 207`,
	}

	js := chain.Filter(mapStr)
	t.Logf("GROK %+v\n", prettyPrint(js))
}

func prettyPrint(m interface{}) string {
	pretty, _:= json.MarshalIndent(m, "", "  ")
	return string(pretty)
}
