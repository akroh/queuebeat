package filter

import (
	"fmt"
	"strings"

	"github.com/andrewkroh/queuebeat/config"
	"github.com/elastic/beats/libbeat/common"
	"github.com/mitchellh/mapstructure"
)

func NewFilterChain(c map[string]interface{}) (FilterChain, error) {
	// TODO: Use reflection here. Have each filter type register itself.
	var chain FilterChain
	for name, ms := range c {
		var ft filterType
		switch strings.ToLower(name) {
		case "json":
			ft.Type = "json"
		case "grok":
			ft.Type = "grok"
		default:
			err := mapstructure.Decode(ms, &ft)
			if err != nil {
				return chain, err
			}
		}

		switch strings.ToLower(ft.Type) {
		default:
			return chain, fmt.Errorf("unknown filter type '%s'", ft.Type)
		case "json":
			var jf JsonFilter
			err := config.Decode(ms, &jf)
			if err != nil {
				return chain, err
			}
			jf.Name = name
			chain.filters = append(chain.filters, jf)
		case "grok":
			var gf GrokFilter
			err := config.Decode(ms, &gf)
			if err != nil {
				return chain, err
			}
			gf.Name = name
			chain.filters = append(chain.filters, gf)
		}
	}

	return chain, nil
}

type filterType struct {
	Type string `mapstructure:"type"`
}

type FilterChain struct {
	filters []Filterer
}

func (f FilterChain) Filter(e common.MapStr) common.MapStr {
	for _, f := range f.filters {
		e = f.Filter(e)
	}
	return e
}

type Filterer interface {
	Filter(common.MapStr) common.MapStr
}

type baseFilter struct {
	Name string
}
