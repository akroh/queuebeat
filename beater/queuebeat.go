package beater

import (
	"expvar"
	"flag"
	"fmt"
	"sync"

	"github.com/andrewkroh/queuebeat/config"
	"github.com/elastic/beats/libbeat/beat"
	"github.com/elastic/beats/libbeat/cfgfile"
	"github.com/elastic/beats/libbeat/logp"
)

// Metrics that can retrieved through the expvar web interface. Metrics must be
// enable through configuration in order for the web service to be started.
var (
	publishedEvents = expvar.NewMap("publishedEvents")
)

// Debug logging functions for this package.
var (
	debugf    = logp.MakeDebug("queuebeat")
	detailf   = logp.MakeDebug("queuebeat_detail")
	memstatsf = logp.MakeDebug("memstats")
)

type Queuebeat struct {
	beat          *beat.Beat
	Configuration *config.Config
	done          chan struct{}
	wg            *sync.WaitGroup
	pubDisabled   bool
}

// Creates beater
func New() *Queuebeat {
	return &Queuebeat{
		done: make(chan struct{}),
	}
}

/// *** Beater interface methods ***///

func (bt *Queuebeat) Config(b *beat.Beat) error {

	// Load beater configuration
	err := cfgfile.Read(&bt.Configuration, "")
	if err != nil {
		return fmt.Errorf("Error reading config file: %v", err)
	}

	logp.Info("Config: %+v", bt.Configuration)

	nFlag := flag.Lookup("N")
	if nFlag != nil && nFlag.Value.String() == "true"{
		bt.pubDisabled = true
		logp.Info("Publishing is disabled")
	}

	return nil
}

func (bt *Queuebeat) Setup(b *beat.Beat) error {
	bt.beat = b
	bt.wg = &sync.WaitGroup{}
	return nil
}

func (bt *Queuebeat) Run(b *beat.Beat) error {
	logp.Info("queuebeat is running! Hit CTRL-C to stop it.")

	for _, sqs := range bt.Configuration.Queuebeat.SQS {
		bt.wg.Add(1)
		go bt.newSQSReader(sqs)
	}

	for _, kafka := range bt.Configuration.Queuebeat.Kafka {
		logp.Warn("Kafka not implemented! Skipping %+v", kafka)
	}

	bt.wg.Wait()
	return nil
}

func (bt *Queuebeat) Cleanup(b *beat.Beat) error {
	logp.Info("Dumping runtime metrics...")
	expvar.Do(func(kv expvar.KeyValue) {
		logf := logp.Info
		if kv.Key == "memstats" {
			logf = memstatsf
		}

		logf("%s=%s", kv.Key, kv.Value.String())
	})
	return nil
}

func (bt *Queuebeat) Stop() {
	close(bt.done)
}
