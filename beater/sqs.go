package beater

import (
	"time"

	"github.com/andrewkroh/queuebeat/config"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/elastic/beats/libbeat/common"
	"github.com/elastic/beats/libbeat/logp"
	"github.com/elastic/beats/libbeat/publisher"
	"github.com/andrewkroh/queuebeat/filter"
)

func (bt *Queuebeat) newSQSReader(config config.SQS) {
	defer bt.wg.Done()

	filterChain, err := filter.NewFilterChain(config.Filters)
	if err != nil {
		return
	}

	publishedEvents.Add(config.Name, 0)

	svc := sqs.New(session.New(), aws.NewConfig().
	WithRegion(config.Region).
	WithCredentials(credentials.NewStaticCredentials(
		config.AWSAccessKey, config.AWSSecretKey, "")))

	params := &sqs.ReceiveMessageInput{
		QueueUrl: aws.String(config.QueueURL), // Required
		AttributeNames: []*string{
			aws.String("QueueAttributeName"), // Required
			aws.String("SentTimestamp"),      // Required
			// More values...
		},
		MaxNumberOfMessages: aws.Int64(10), // Max is 10
		MessageAttributeNames: []*string{
			aws.String("MessageAttributeName"), // Required
			// More values...
		},
		VisibilityTimeout: aws.Int64(config.VisibilityTimeoutSec),
		WaitTimeSeconds:   aws.Int64(config.WaitTimeoutSec),
	}

	for {
		select {
		case <-bt.done:
			return
		default:
		}

		resp, err := svc.ReceiveMessage(params)
		if err != nil {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			logp.Err("%v", err)
			return
		}

		receiveTime := time.Now()
		var deletes []*sqs.DeleteMessageBatchRequestEntry
		for _, m := range resp.Messages {
			event := common.MapStr{
				"@timestamp": common.Time(receiveTime),
				"type":       config.Type,
				"name":       config.Name,
				"sqs":        *m.Body,
			}
			bt.beat.Events.PublishEvent(filterChain.Filter(event), publisher.Sync, publisher.Guaranteed)
			publishedEvents.Add(config.Name, int64(len(resp.Messages)))

			deletes = append(deletes, &sqs.DeleteMessageBatchRequestEntry{
				Id: m.MessageId,
				ReceiptHandle: m.ReceiptHandle,
			})
		}

		if !bt.pubDisabled {
			debugf("[%s] Deleting messages %d from queue", config.Name, len(deletes))

			params := &sqs.DeleteMessageBatchInput{
				QueueUrl: aws.String(config.QueueURL), // Required
				Entries:  deletes,
			}

			resp, err := svc.DeleteMessageBatch(params)
			if err != nil {
				// Print the error, cast err to awserr.Error to get the Code and
				// Message from an error.
				logp.Err("%v", err)
				return
			}

			debugf("[%s] delete response %+v", config.Name, resp)
		}
	}
}
